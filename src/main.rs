use std::io;
use std::io::prelude::*;

fn main() {
    println!("Starting BakeryDB...");
    let mut db = Vec::<String>::new();
    println!("BakeryDB has been initialized.");
    println!("Waiting for input.");

    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.unwrap();

        if line == "exit" {
            println!("Shutting down BakeryDB...");
            break;
        }

        db.push(line);
        println!("Added `{}` to the database. Current database state:", db[db.len() - 1]);
        for elem in &db {
            println!("{}", elem)
        }
    }

    println!("Cleaning BakeryDB...");
}
